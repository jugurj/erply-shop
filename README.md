# ErplyShop

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) v6.0.0.

Styled with Bootstrap v4.1.1. and Font Awesome Icons.

Shopping cart storage realised with Firebase v4.2.0 and AngulrFire2 v4.0.0-rc.1.


## Project Running

Run `npm install` command in downloaded/cloned directory to download packages.

Run `ng serve` to run local server. Navigate to `http://localhost:4200/`.


## Web Application Structure

Home page displays all product from Erply API as cards.

Below navigation bar you can sort items by category/avalability.

Add product to cart by clicking shopping cart icon on the right bottom side of the card.

You can see detailed info by clicking the card title.

The shopping cart icon on the navigation bar directs to shopping page where all products in you cart are displayed.

You can remove items from cart by clicking "remove" button or clear all cart with "clear shopping cart" button.

