import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from '../shopping-cart.service';
import { Product } from '../product.service';

@Component({
  selector: 'bs-navbar',
  templateUrl: './bs-navbar.component.html',
  styleUrls: ['./bs-navbar.component.css']
})
export class BsNavbarComponent implements OnInit {

  shoppingCartTotal: number;

  constructor(private shoppingCartService: ShoppingCartService) { }

  async ngOnInit() {
    let shoppingCart$ = await this.shoppingCartService.getShoppingCart()
    shoppingCart$.subscribe(cart => {
      this.shoppingCartTotal = 0;
      for ( let productId in cart.products)
        this.shoppingCartTotal += cart.products[productId].quantity;
    });
  }

}
