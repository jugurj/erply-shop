import { ProductService, Product } from './../product.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ShoppingCartService } from '../shopping-cart.service';

@Component({
  selector: 'product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  product = {};
  shoppingCart;
  productId;

  constructor(private productService: ProductService,
    private shoppingCartService: ShoppingCartService,
    private route: ActivatedRoute) { 
      this.productId = this.route.snapshot.paramMap.get('id');
      this.productService.getProducts().subscribe(
        data => {
          this.product = data.filter(p => p.productcode === this.productId)[0];
        });
  }

  async ngOnInit() {
    (await this.shoppingCartService.getShoppingCart())
      .subscribe(cart => this.shoppingCart = cart);
  }
  
  isInStock(product: Product) {
    return product.instock ? true : false;
  }

  addToCart(product) {
    this.shoppingCartService.addToShoppingCart(product);
  }

  getQuantity() {
    if (!this.shoppingCart) return 0;
    if (this.shoppingCart.products != undefined) {
      let product = this.shoppingCart.products[this.productId];
      return product ? product.quantity : 0;
    }
    return 0;
  }

}
