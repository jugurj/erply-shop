import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

export interface Product {
  currency;
  department;
  description;
  id;
  image;
  instock;
  name;
  price;
  productcode;
  store;
}

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  apiUrl = "https://erply-challenge.herokuapp.com/list";
  apiKey = "fae7b9f6-6363-45a1-a9c9-3def2dae206d";
  apiKeySign = "AUTH";

  constructor(private http:HttpClient) { }

  getProducts() {
    return this.http.get<Product[]>(this.apiUrl + '?' + this.apiKeySign + '=' + this.apiKey );
  }

}
