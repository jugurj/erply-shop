import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'categories-filter',
  templateUrl: './categories-filter.component.html',
  styleUrls: ['./categories-filter.component.css']
})
export class CategoriesFilterComponent implements OnInit {

  @Input('categories') categories;
  @Input('category') category;

  constructor() {
  }

  ngOnInit() {
  }

}
