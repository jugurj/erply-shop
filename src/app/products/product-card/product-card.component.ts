import { Component, Input } from '@angular/core';
import { ShoppingCartService } from '../../shopping-cart.service';

@Component({
  selector: 'product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent {

  @Input('product') product;
  @Input('shopping-cart') shoppingCart;

  constructor(private shoppingCartService: ShoppingCartService) { }

  addToCart(product) {
    this.shoppingCartService.addToShoppingCart(product);
  }

  getQuantity() {
    if (!this.shoppingCart) return 0; 
    if (this.shoppingCart.products != undefined) {
      let product = this.shoppingCart.products[this.product.productcode];
      return product ? product.quantity : 0;
    }
    return 0;
  }

}
