import { ShoppingCartService } from './../shopping-cart.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProductService, Product } from '../product.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit, OnDestroy {

  products: Product[] = [];
  filteredProducts = [];
  category;
  categories = new Set();
  inStock = [true, false];
  inStockLabel = "Show in Stock";
  shoppingCart;
  subscribtion: Subscription;

  constructor(private route: ActivatedRoute,
    private productService: ProductService,
    private shoppingCartService: ShoppingCartService) { 
    
    this.productService.getProducts().subscribe(
      data => { this.products = data },
      err => console.error("API data downloading failed."),
      () => { 
        console.log('API data downloaded successfully.');
        // Generating list of unique categories...
        this.getCategories();
        // Subscribing to get query params...
        this.filterList();
      }
    );
  }

  async ngOnInit() {
    this.subscribtion = (await this.shoppingCartService.getShoppingCart())
      .subscribe(cart => this.shoppingCart = cart);
  }

  getCategories() {
    for ( let product in this.products)
      this.categories.add(this.products[product].department);
  }

  filterList() {
    this.route.queryParamMap.subscribe(params => {
      this.category = params.get('category');
      
      this.filteredProducts = (this.category) ? 
        this.products.filter(prod => 
          (prod.department === this.category) && 
          (this.inStock.includes(prod.instock)) ) :
          this.products.filter(prod =>
            (this.inStock.includes(prod.instock)));
    })
  }

  changeStock() {
    if (this.inStock.length > 1) {
      this.inStock.splice(1,1);
      this.inStockLabel = "Show All";
    } else {
      this.inStock.push(false);
      this.inStockLabel = "Show in Stock";
    }
    this.filterList();
  }

  ngOnDestroy(): void {
    this.subscribtion.unsubscribe();
  }

}
