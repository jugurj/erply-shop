import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';
import { Product } from './product.service';
import 'rxjs/add/operator/take';
import { ShoppingCart } from './shopping-cart/shopping-cart';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {

  constructor(private db: AngularFireDatabase) { }

  async getShoppingCart(): Promise<FirebaseObjectObservable<ShoppingCart>> {
    let shoppingCartId = await this.getOrCreateShoppingCartId();
    return this.db.object('/shopping-cart/' + shoppingCartId);
  }

  async addToShoppingCart(product: Product) {
    this.updateItemQuantity(product, 1);
  }

  async removeFromShoppingCart(product: Product) {
    this.updateItemQuantity(product, -1);
  }

  async clearShoppingCart() {
    let shoppingCartId = await this.getOrCreateShoppingCartId();
    this.db.object('/shopping-cart/' + shoppingCartId + '/products').remove();
  }

  private createShoppingCart() {
    return this.db.list('/shopping-cart').push({
      dateCreated: new Date().getTime()
    });
  }

  private getProduct(shoppingCartId: string, productId: string) {
    return this.db.object('/shopping-cart/' + shoppingCartId + '/products/' + productId);
  }

  private async getOrCreateShoppingCartId():Promise<string> {
    let shoppingCartId = localStorage.getItem('shoppingCartId');

    if (shoppingCartId) return shoppingCartId;

    let result = await this.createShoppingCart();
    localStorage.setItem('shoppingCartId', result.key);
    return result.key;
  }

  private async updateItemQuantity(product: Product, change: number) {
    let shoppingCartId = await this.getOrCreateShoppingCartId();
    let product$ = this.getProduct(shoppingCartId, product.productcode);
    product$.take(1).subscribe(item => {
      if ((item.quantity + change) === 0) product$.remove();
      product$.update({ product: product, quantity: (item.quantity || 0) + change });
    });
  }

}
