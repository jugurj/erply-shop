import { Product } from "../product.service";

export interface ShoppingCartProduct {
    product: Product,
    quantity: number
}