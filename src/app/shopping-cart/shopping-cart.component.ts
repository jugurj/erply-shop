import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from '../shopping-cart.service';
import { Product } from '../product.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
  shoppingCart$;
  shoppingCartTotal: number;
  productsIds = [];
  totalCartPrice = 0;

  constructor(private shoppingCartService: ShoppingCartService) { }

  async ngOnInit() {
    this.shoppingCart$ = await this.shoppingCartService.getShoppingCart();

    this.shoppingCart$.subscribe(cart => {
      this.shoppingCartTotal = 0;
      this.totalCartPrice = 0;
      this.productsIds = [];
      for ( let productId in cart.products) {
        let productItem = cart.products[productId];

        if(productItem.quantity > 0) {
          this.productsIds.push(productId);
          this.shoppingCartTotal += productItem.quantity;
          this.totalCartPrice += (productItem.quantity *
            productItem.product.price);
        }
      }
    });
  }

  removeFromCart(product: Product) {
    this.shoppingCartService.removeFromShoppingCart(product);
  }

  clearShopingCart() {
    this.shoppingCartService.clearShoppingCart();
  }

}
